import 'package:othelloproject/othelloproject.dart' as othelloproject;

import 'Game.dart';

void main(List<String> arguments) {
  Game game = new Game();
  game.clearScreen();
  game.showText();
  game.newboard();
  while (true) {
    game.clearScreen();
    game.showStat();
    game.showTable();
    game.showTurn();
    game.input();
    game.clearScreen();
    game.showStat();
    game.showTable();
    if(game.checkTurn()||game.isEnd()){
      game.clearScreen();
      game.showStat();
      game.showTable();
      game.showResult();
      game.newboard();
    }
    
  }
}
