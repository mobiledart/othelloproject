import 'dart:io';

import 'Player.dart';

class Board {
  late Player w;
  late Player b;
  int count = 0;
  var currentPlayer = 'W';
  int countCheck = 0;
  var table = [
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', 'B', 'B', ' ', ' ', ' '],
    [' ', ' ', ' ', 'B', 'B', 'B', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', 'W', ' ', ' '],
    [' ', ' ', ' ', ' ', ' ', 'B', ' ', 'W'],
    [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
  ];

  Board(Player w, Player b) {
    this.w = w;
    this.b = b;
  }

  getTable() {
    return table;
  }

  setTable(String rowcol) {
    rowcol.toUpperCase();
    if (rowcol.length < 2) return false;
    if (checkCol(rowcol)) {
      switchPlayer();
      return true;
    } else {
      print("\x1B[2J\x1B[0;0H");
      print("\u001b[31;1m███████╗██████╗░██████╗░░█████╗░██████╗░  ██╗██╗");
      print("██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔══██╗  ██║██║");
      print("█████╗░░██████╔╝██████╔╝██║░░██║██████╔╝  ██║██║");
      print("██╔══╝░░██╔══██╗██╔══██╗██║░░██║██╔══██╗  ╚═╝╚═╝");
      print("███████╗██║░░██║██║░░██║╚█████╔╝██║░░██║  ██╗██╗");
      print("╚══════╝╚═╝░░╚═╝╚═╝░░╚═╝░╚════╝░╚═╝░░╚═╝  ╚═╝╚═╝\u001b[0m");
      sleep(const Duration(seconds: 2));
    }
    return false;
  }

  switchPlayer() {
    if (currentPlayer == 'W') {
      currentPlayer = 'B';
    } else {
      currentPlayer = 'W';
    }
  }

  checkCol(String rowcol) {
    if (rowcol[0] == 'A') {
      int x = 0;
      return checkrow(rowcol, x);
    } else if (rowcol[0] == 'B') {
      int x = 1;
      return checkrow(rowcol, x);
    } else if (rowcol[0] == 'C') {
      int x = 2;
      return checkrow(rowcol, x);
    } else if (rowcol[0] == 'D') {
      int x = 3;
      return checkrow(rowcol, x);
    } else if (rowcol[0] == 'E') {
      int x = 4;
      return checkrow(rowcol, x);
    } else if (rowcol[0] == 'F') {
      int x = 5;
      return checkrow(rowcol, x);
    } else if (rowcol[0] == 'G') {
      int x = 6;
      return checkrow(rowcol, x);
    } else if (rowcol[0] == 'H') {
      int x = 7;
      return checkrow(rowcol, x);
    }
    return false;
  }

  checkrow(String rowcol, int x) {
    if (rowcol[1] == '1') {
      if (table[0][x] == ' ' && checkAll(0, x,1)) {
        table[0][x] = currentPlayer;
        return true;
      }
    } else if (rowcol[1] == '2') {
      if (table[1][x] == ' ' && checkAll(1, x,1)) {
        table[1][x] = currentPlayer;
        return true;
      }
    } else if (rowcol[1] == '3') {
      if (table[2][x] == ' ' && checkAll(2, x,1)) {
        table[2][x] = currentPlayer;
        return true;
      }
    } else if (rowcol[1] == '4') {
      if (table[3][x] == ' ' && checkAll(3, x,1)) {
        table[3][x] = currentPlayer;
        return true;
      }
    } else if (rowcol[1] == '5') {
      if (table[4][x] == ' ' && checkAll(4, x,1)) {
        table[4][x] = currentPlayer;
        return true;
      }
    } else if (rowcol[1] == '6') {
      if (table[5][x] == ' ' && checkAll(5, x,1)) {
        table[5][x] = currentPlayer;
        return true;
      }
    } else if (rowcol[1] == '7') {
      if (table[6][x] == ' ' && checkAll(6, x,1)) {
        table[6][x] = currentPlayer;
        return true;
      }
    } else if (rowcol[1] == '8') {
      if (table[7][x] == ' ' && checkAll(7, x,1)) {
        table[7][x] = currentPlayer;
        return true;
      }
    }
    return false;
  }

  checkAll(int row, int col, int mode) {
    if (checkNorth(row, col, mode)) {
      countCheck++;
    }
    if (checkSouth(row, col, mode)) {
      countCheck++;
    }
    if (checkWest(row, col, mode)) {
      countCheck++;
    }
    if (checkEast(row, col, mode)) {
      countCheck++;
    }
    if (checkNorthWest(row, col, mode)) {
      countCheck++;
    }
    if (checkSouthwest(row, col, mode)) {
      countCheck++;
    } 
    if (checkNortheast(row, col, mode)) {
      countCheck++;
    } 
    if (checkSoutheast(row, col, mode)) {
      countCheck++;
    } 

    if (countCheck > 0) {
      countCheck = 0;
      return true;
    }
    return false;
  }

  bool checkNorth(int row, int col,int mode) {
    int start = -1;
    int stop = -1;
    for (int i = row - 1; i > -1; i--) {
      if (table[i][col] == ' ' || (table[i][col] == currentPlayer && start == -1)) {
        return false;
      } else if (table[i][col] != currentPlayer && start == -1) {
        start = i;
      } else if (table[i][col] == currentPlayer && start != -1) {
        stop = i;
        break;
      }
    }
    if (start != -1 && stop != -1 && mode == 1) {
      for (int i = start; i > stop; i--) {
        table[i][col] = currentPlayer;
      }
      return true;
    }
    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }

  bool checkSouth(int row, int col,int mode) {
    int start = -1;
    int stop = -1;
    for (int i = row + 1; i < 8; i++) {
      if (table[i][col] == ' '|| (table[i][col] == currentPlayer && start == -1)) {
        return false;
      } else if (table[i][col] != currentPlayer && start == -1) {
        start = i;
      } else if (table[i][col] == currentPlayer && start != -1) {
        stop = i;
        break;
      }
    }
    if (start != -1 && stop != -1&& mode == 1) {
      for (int i = start; i < stop; i++) {
        table[i][col] = currentPlayer;
      }
      return true;
    }

    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }

  bool checkWest(int row, int col,int mode) {
    int start = -1;
    int stop = -1;
    for (int i = col - 1; i > -1; i--) {
      if (table[row][i] == ' '|| (table[row][i] == currentPlayer && start == -1)) {
        return false;
      } else if (table[row][i] != currentPlayer && start == -1) {
        start = i;
      } else if (table[row][i] == currentPlayer && start != -1) {
        stop = i;
        break;
      }
    }
    if (start != -1 && stop != -1 && mode == 1) {
      for (int i = start; i > stop; i--) {
        table[row][i] = currentPlayer;
      }
      return true;
    }
    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }

  bool checkEast(int row, int col,int mode) {
    int start = -1;
    int stop = -1;
    for (int i = col + 1; i < 8; i++) {
      if (table[row][i] == ' '|| (table[row][i] == currentPlayer && start == -1 )) {
        return false;
      } else if (table[row][i] != currentPlayer && start == -1) {
        start = i;
      } else if (table[row][i] == currentPlayer && start != -1) {
        stop = i;
        break;
      }
    }
    if (start != -1 && stop != -1 && mode == 1) {
      for (int i = start; i < stop; i++) {
        table[row][i] = currentPlayer;
      }
      return true;
    }

    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }

  bool checkNorthWest(int row, int col,int mode) {
    int start = -1;
    int stop= -1;
    int saverow = row-1;
    int savecol = col-1;
    int count = 0;
    row--;
    col--;
    for (int i = 0; i < 8; i++) {
      if (row > -1 && col > -1) {
        if (table[row][col] == ' '|| (table[row][col] == currentPlayer && start == -1)) {
          return false;
        } else if (table[row][col] != currentPlayer && start == -1) {
          start= 1;
        } else if (table[row][col] == currentPlayer && start != -1) {
          stop= 1;
          break;
        }
        count++;
      } else {
        break;
      }
      row--;
      col--;
    }
    if (start != -1 && stop != -1 && mode == 1) {
      for (int i = 0; i < count; i++) {
        table[saverow][savecol] = currentPlayer;
        saverow--;
        savecol--;
      }
      return true;
    }

    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }

  bool checkSouthwest(int row, int col,int mode) {
    int start = -1;
    int stop= -1;
    int saverow = row+1;
    int savecol = col-1;
    int count = 0;
    row++;
    col--;
    for (int i = 0; i < 8; i++) {
      if (row < 8 && col > -1) {
        if (table[row][col] == ' '|| (table[row][col] == currentPlayer && start == -1)) {
          return false;
        } else if (table[row][col] != currentPlayer && start == -1) {
          start= 1;
        } else if (table[row][col] == currentPlayer && start != -1) {
          stop= 1;
          break;
        }
        count++;
      } else {
        break;
      }
      row++;
      col--;
    }
    if (start != -1 && stop != -1 && mode == 1) {
      for (int i = 0; i < count; i++) {
        table[saverow][savecol] = currentPlayer;
        saverow++;
        savecol--;
      }
      return true;
    }

    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }

  bool checkNortheast(int row, int col,int mode) {
    int start = -1;
    int stop= -1;
    int saverow = row-1;
    int savecol = col+1;
    int count = 0;
    row--;
    col++;
    for (int i = 0; i < 8; i++) {
      if (row > -1 && col < 8) {
        if (table[row][col] == ' '|| (table[row][col] == currentPlayer && start == -1)) {
          return false;
        } else if (table[row][col] != currentPlayer && start == -1) {
          start= 1;
        } else if (table[row][col] == currentPlayer && start != -1) {
          stop= 1;
          break;
        }
        count++;
      } else {
        break;
      }
      row--;
      col++;
    }
    if (start != -1 && stop != -1 && mode == 1) {
      for (int i = 0; i < count; i++) {
        table[saverow][savecol] = currentPlayer;
        saverow--;
        savecol++;
      }
      return true;
    }

    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }

  bool checkSoutheast(int row, int col,int mode) {
    int start = -1;
    int stop= -1;
    int saverow = row+1;
    int savecol = col+1;
    int count = 0;
    row++;
    col++;
    for (int i = 0; i < 8; i++) {
      if (row < 8 && col < 8) {
        if (table[row][col] == ' '|| (table[row][col] == currentPlayer && start == -1)) {
          return false;
        } else if (table[row][col] != currentPlayer && start == -1) {
          start= 1;
        } else if (table[row][col] == currentPlayer && start != -1) {
          stop= 1;
          break;
        }
        count++;
      } else {
        break;
      }
      row++;
      col++;
    }
    if (start != -1 && stop != -1 && mode == 1) {
      for (int i = 0; i < count; i++) {
        table[saverow][savecol] = currentPlayer;
        saverow++;
        savecol++;
      }
      return true;
    }

    if (start != -1 && stop != -1 && mode == 2) {
      return true;
    }
    return false;
  }
}
