import 'dart:ffi';

class Player{
  var symbol;
  int win=0;
  int loss=0;
  int draw=0;

  Player(var symbol){
    this.symbol = symbol;
  }
  
  String getSymbol(){
    return symbol;
  }

  int getWin(){
    return win;
  }

  int getLoss(){
    return loss;
  }

  int getDraw(){
    return draw;
  }

  void Win(){
    this.win++;
  }

  void Loss(){
    this.loss++;
  }

  void Draw(){
    this.draw++;
  }

  @override
  String toString() {
    return "Player : "+ symbol + "   Win = " + win.toString() + " Loss = " + loss.toString() + " Draw = " + draw.toString();
  }


}