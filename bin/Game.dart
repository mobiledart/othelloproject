import 'dart:io';
import 'Board.dart';
import 'Player.dart';

class Game {
  late Player w;
  late Player b;
  int Black = 0;
  int White = 0;  
  late Board board;

  Game(){
    this.w = new Player('W');
    this.b = new Player('B');
  }

  newboard(){
    this.board = new Board(w, b);
  }

  showText() {
    print(
        "\u001b[31;1m░█████╗░\u001b[33;1m████████╗\u001b[32;1m██╗░░██╗\u001b[34;1m███████╗\u001b[38;5;198m██╗░░░░░\u001b[38;5;200m██╗░░░░░\u001b[38;5;165m░█████╗░  \u001b[38;5;85m░██████╗░\u001b[38;5;209m░█████╗░\u001b[38;5;224m███╗░░░███╗\u001b[38;5;33m███████╗");
    print(
        "\u001b[31;1m██╔══██╗\u001b[33;1m╚══██╔══╝\u001b[32;1m██║░░██║\u001b[34;1m██╔════╝\u001b[38;5;198m██║░░░░░\u001b[38;5;200m██║░░░░░\u001b[38;5;165m██╔══██╗  \u001b[38;5;85m██╔════╝░\u001b[38;5;209m██╔══██╗\u001b[38;5;224m████╗░████║\u001b[38;5;33m██╔════╝");
    print(
        "\u001b[31;1m██║░░██║\u001b[33;1m░░░██║░░░\u001b[32;1m███████║\u001b[34;1m█████╗░░\u001b[38;5;198m██║░░░░░\u001b[38;5;200m██║░░░░░\u001b[38;5;165m██║░░██║  \u001b[38;5;85m██║░░██╗░\u001b[38;5;209m███████║\u001b[38;5;254m██╔████╔██║\u001b[38;5;33m█████╗░░");
    print(
        "\u001b[31;1m██║░░██║\u001b[33;1m░░░██║░░░\u001b[32;1m██╔══██║\u001b[34;1m██╔══╝░░\u001b[38;5;198m██║░░░░░\u001b[38;5;200m██║░░░░░\u001b[38;5;165m██║░░██║  \u001b[38;5;85m██║░░╚██╗\u001b[38;5;209m██╔══██║\u001b[38;5;224m██║╚██╔╝██║\u001b[38;5;33m██╔══╝░░");
    print(
        "\u001b[31;1m╚█████╔╝\u001b[33;1m░░░██║░░░\u001b[32;1m██║░░██║\u001b[34;1m███████╗\u001b[38;5;198m███████╗\u001b[38;5;200m███████╗\u001b[38;5;165m╚█████╔╝  \u001b[38;5;85m╚██████╔╝\u001b[38;5;209m██║░░██║\u001b[38;5;224m██║░╚═╝░██║\u001b[38;5;33m███████╗");
    print(
        "\u001b[31;1m░╚════╝░\u001b[33;1m░░░╚═╝░░░\u001b[32;1m╚═╝░░╚═╝\u001b[34;1m╚══════╝\u001b[38;5;198m╚══════╝\u001b[38;5;200m╚══════╝\u001b[38;5;165m░╚════╝░  \u001b[38;5;85m░╚═════╝░\u001b[38;5;209m╚═╝░░╚═╝\u001b[38;5;254m╚═╝░░░░░╚═╝\u001b[38;5;33m╚══════╝");
    print("\u001b[37;1mHow To Play");
    print(
        "1. There will be a total of 64 channels, 2 players take turns playing.\n2. The player must input the desired field, for example E6.and must be able to clamp the opposite side\n3. The body that has been clamped will change into the person of the other party\n4. If you can't get down The program will change to the Turn of the other side.\n5. End of game, which side has more will win. If equal, always");
    print("Press Enter To Play.\u001b[0m");
    String? Enter = stdin.readLineSync();
  }

  clearScreen() {
    print("\x1B[2J\x1B[0;0H");
  }

  showTable(){
    var table = board.getTable();
    print("     \u001b[37;1mA   B   C   D   E   F   G   H\u001b[0m  ");
    for (int r = 0; r < table.length; r++) {
      print("   \u001b[42;1m+---+---+---+---+---+---+---+---+\u001b[0m");
      for (int c = 0; c < table[r].length; c++) {
        if(c == 0){
          stdout.write("\u001b[37;1m"+(r+1).toString()+"  \u001b[0m");
          
        }
        printGreen("| ");
        printTable(table[r][c]);
        if(c == table[r].length-1){
          printGreen("|");
        }
      }
      print("");
    }
    print("   \u001b[42;1m+---+---+---+---+---+---+---+---+\u001b[0m");
  }

  void printGreen(String text) {
  stdout.write('\u001b[42;1m$text\u001b[0m');
  }

  void printWhite(String text) {
  stdout.write('\u001b[47m$text\x1B[0m'+"\u001b[42;1m \x1B[0m");
  }

  void printBlack(String text) {
  stdout.write('\u001b[40m$text\x1B[0m'+"\u001b[42;1m \x1B[0m");
  }

  printTable(String table){
    if(table == 'W'){
      printWhite(table);
    }else if(table == 'B'){
      printBlack(table);
    }else{
      stdout.write("\u001b[42;1m  \x1B[0m");
    }
  }
  
  showTurn(){
    print("\nTurn : "+board.currentPlayer);
  }

  input(){
    stdout.write("Please Input : ");
    String? rowcol = stdin.readLineSync();
    if(board.setTable(rowcol!.toUpperCase())){
      board.count++;
    }
  }

  showStat(){
    var table = board.getTable();
    Black = 0;
    White = 0;
    for (int r = 0; r < table.length; r++) {
      for (int c = 0; c < table[r].length; c++) {
        if(table[r][c] == 'B'){
          Black++;
        }else if(table[r][c] == 'W'){
          White++;
        }
      }
    }
    print("--------------------------------------");
    print(w.toString());
    print(b.toString());
    print("--------------------------------------");
    print("          Black : "+Black.toString() + " White : "+White.toString()+"\n");
  }

 bool isEnd(){
  if (board.count == 62){
    return true;
  }
  return false;
 }

 showResult(){
  if(Black > White){
    b.Win();
    w.Loss();
    showTextWin('b');
  }else if(Black < White){
    b.Loss();
    w.Win();
    showTextWin('w');
  }else{
    b.Draw();
    w.Draw();
    print("");
    print("░█▀▀▄ ░█▀▀█ ─█▀▀█ ░█──░█ 　 █ █ ");
    print("░█─░█ ░█▄▄▀ ░█▄▄█ ░█░█░█ 　 ▀ ▀ ");
    print("░█▄▄▀ ░█─░█ ░█─░█ ░█▄▀▄█ 　 ▄ ▄");
    print("\nPress Enter To NextGame.");
    String? Enter = stdin.readLineSync();

  }
 }

 showTextWin(String x){
  String x1 ="";
  String x2 ="";
  String x3 ="";
  if(x == 'b'){
    x1="░█▀▀█ ░█─── ─█▀▀█ ░█▀▀█ ░█─▄▀";
    x2="░█▀▀▄ ░█─── ░█▄▄█ ░█─── ░█▀▄─";
    x3="░█▄▄█ ░█▄▄█ ░█─░█ ░█▄▄█ ░█─░█";
  }else{
    x1="░█──░█ ░█─░█ ▀█▀ ▀▀█▀▀ ░█▀▀▀";
    x2="░█░█░█ ░█▀▀█ ░█─ ─░█── ░█▀▀▀";
    x3="░█▄▀▄█ ░█─░█ ▄█▄ ─░█── ░█▄▄▄";
  }
  print("");
  print("░█▀▀█ ░█─── ─█▀▀█ ░█──░█ ░█▀▀▀ ░█▀▀█  "+x1+"  ░█──░█ ▀█▀ ░█▄─░█");
  print("░█▄▄█ ░█─── ░█▄▄█ ░█▄▄▄█ ░█▀▀▀ ░█▄▄▀  "+x2+"  ░█░█░█ ░█─ ░█░█░█");
  print("░█─── ░█▄▄█ ░█─░█ ──░█── ░█▄▄▄ ░█─░█  "+x3+"  ░█▄▀▄█ ▄█▄ ░█──▀█");
  print("\nPress Enter To NextGame.");
  String? Enter = stdin.readLineSync();
 }
  
  bool checkTurn(){
    var table = board.getTable();
    for (int r = 0; r < table.length; r++) {
      for (int c = 0; c < table[r].length; c++) {
        if(table[r][c] == ' '){
          if(board.checkAll(r, c,2)){
            return false;
          }
        }
      }
    }
    board.switchPlayer();

    for (int r = 0; r < table.length; r++) {
      for (int c = 0; c < table[r].length; c++) {
        if(table[r][c] == ' '){
          if(board.checkAll(r, c,2)){
            if(board.currentPlayer == 'W'){
              print("");
              print("░█▀▀▀█ ░█─▄▀ ▀█▀ ░█▀▀█ 　 ▀▀█▀▀ ░█▀▀█ ░█─░█ ░█▄─░█ 　 ░█▀▀█ ░█─── ─█▀▀█ ░█▀▀█ ░█─▄▀");
              print("─▀▀▀▄▄ ░█▀▄─ ░█─ ░█▄▄█ 　 ─░█── ░█▄▄▀ ░█─░█ ░█░█░█ 　 ░█▀▀▄ ░█─── ░█▄▄█ ░█─── ░█▀▄─");
              print("░█▄▄▄█ ░█─░█ ▄█▄ ░█─── 　 ─░█── ░█─░█ ─▀▄▄▀ ░█──▀█ 　 ░█▄▄█ ░█▄▄█ ░█─░█ ░█▄▄█ ░█─░█");
              sleep(const Duration(seconds: 2));
            }else{
              print("");
              print("░█▀▀▀█ ░█─▄▀ ▀█▀ ░█▀▀█ 　 ▀▀█▀▀ ░█▀▀█ ░█─░█ ░█▄─░█ 　 ░█──░█ ░█─░█ ▀█▀ ▀▀█▀▀ ░█▀▀▀ ");
              print("─▀▀▀▄▄ ░█▀▄─ ░█─ ░█▄▄█ 　 ─░█── ░█▄▄▀ ░█─░█ ░█░█░█ 　 ░█░█░█ ░█▀▀█ ░█─ ─░█── ░█▀▀▀ ");
              print("░█▄▄▄█ ░█─░█ ▄█▄ ░█─── 　 ─░█── ░█─░█ ─▀▄▄▀ ░█──▀█ 　 ░█▄▀▄█ ░█─░█ ▄█▄ ─░█── ░█▄▄▄");
              sleep(const Duration(seconds: 2));
            }
            return false;
          }
        }
      }
    }
    return true;
  }
}
